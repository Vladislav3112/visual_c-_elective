#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

static int max = 0;
void dfs(int v, vector<vector<int>> &data, vector<int> &vs)
{
	static int cnt = -1;
	++cnt;

	for (auto i = data[v].begin(); i != data[v].end(); ++i) {
		dfs(*i, data, vs);
	}

	if (cnt > ::max) {
		::max = cnt;
		vs.clear();
		vs.push_back(v);
	}
	else if (cnt == ::max)
		vs.push_back(v);

	--cnt;
}

int main()
{
	int n;
	cin >> n;
	vector<vector<int>> data(n + 1);
	vector<int> vs;

	for (int i = 2; i <= n; ++i)
	{
		int tmp; cin >> tmp;
		data[tmp].push_back(i);
	}

	dfs(1, data, vs);

	cout <<::max << endl;
	cout << vs.size() << endl;
	std::sort(vs.begin(), vs.end());
	for (auto i = vs.begin(); i != vs.end(); ++i)
		cout << *i << ' ';
	cout << endl;

	return 0;
}