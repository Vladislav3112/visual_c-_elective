#include<iostream>
#include<vector>
#include <algorithm>
#include <iterator>
using namespace std;
bool comp2(int a, int b) { return (a>b); }

int main() {
	
	int n,k;
	int tmp;
	int res[10] = { 0 };
	cin >> k;
	
	vector<int>x, y;
	
	for (int j = 0; j < k; j++) {
		cin >> n;

		for (int i = 0; i < n; i++) {
			cin >> tmp;
			x.push_back(tmp);
		}
		for (int i = 0; i < n; i++) {
			cin >> tmp;
			y.push_back(tmp);
		}
		
		sort(x.begin(), x.end());
		sort(y.begin(), y.end(), comp2);
		vector<int>::iterator it1, it2;
		for (it1 = x.begin(), it2 = y.begin();
			it1 <= x.end() - 1, it2 <= y.end() - 1; it1++, it2++)
		{
			res[j] += (*it1) * (*it2);

		}
		x.clear();
		y.clear();

	}
	
	for (int i = 0; i < k; i++) {
		cout << "Case #" << i + 1 << ": " << res[i];
		cout << "\n";
	}

	system("pause");
	return 0;
}
